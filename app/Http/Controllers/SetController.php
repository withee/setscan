<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class SetController extends Controller
{
    //
    public function find(Request $request)
    {

        $pe = $request->input('pe');
        $pbv = $request->input('pbv');
        $year = Carbon::now()->year;

        $currentday = \DB::select('SELECT MAX(DATE(st.created_at)) as day FROM statistic st');
        $day = $currentday[0]->day;
        $list = array();
        if (!empty($pe) && !empty($pbv)) {
            $list = \DB::select('SELECT * FROM statistic st
LEFT JOIN symbol s ON st.symbol=s.id
WHERE st.pe<=?
AND st.pe>0
AND st.pvb<=?
AND st.year=?
AND DATE(st.created_at)=?
ORDER BY st.pe',[$pe,$pbv,$year,$day]);
        }

//echo $currentday[0]->day;
        //var_dump($currentday);
//       $users = \DB::select('select * from  where active = ?', [1]);

        return view('search', ['list' => $list, 'day' => $day]);
    }
}
