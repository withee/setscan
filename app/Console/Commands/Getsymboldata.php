<?php

namespace App\Console\Commands;

use App\Period;
use App\Statistic;
use App\Symbol;
use Illuminate\Console\Command;

class Getsymboldata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:getsymboldata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $symbols = Symbol::all();

        foreach ($symbols as $symbol) {
            try {
                echo $symbol->name . "\n";
                $url = "http://www.set.or.th/set/companyhighlight.do?symbol={$symbol->name}&ssoPageId=5&language=th&country=TH";

                $rshearder = get_headers($url);
                echo $url . "\n";
                echo $rshearder[0], "\n";
                //var_dump($rshearder);exit;
                if (strpos($rshearder[0], "200")) {
                    $page = file_get_contents($url);
                    $contentp1 = explode("<div class=\"table-responsive\">", $page);
                    $contentp2 = explode("<div class=\"remark-content\">", $contentp1[1]);

                    $rawtable = preg_replace("/<\\/div>/", "", $contentp2[0]);

                    //echo $rawtable;
                    //exit;
                    $html = new \Htmldom($rawtable);
                    foreach ($html->find(".table-info") as $key => $table) {
                        //echo $tables->outertext;
                        //exit;
                        //$temptable = preg_replace("/<\\/div>/", $tables->outertext);
                        $periodlist = array();
                        $statisticlist = array();
                        foreach ($table->find("thead") as $theadkey => $thead) {
                            if ($theadkey == 0) {
                                foreach ($thead->children(0)->children() as $thkey => $th) {
                                    if ($thkey > 0 && $thkey < 5) {
                                        //echo $th->innertext . "\n";
                                        $temperiod = new Period();
                                        $temperiod->label = preg_replace("/&nbsp;|<br>/", " ", trim($th->children(0)->innertext));
                                        echo $temperiod->label . "\n";
                                        $temperiod->symbol = $symbol->id;
                                        $yearflag = explode("/", $temperiod->label);
                                        $temperiod->year = (int)$yearflag[count($yearflag) - 1] - 543;
                                        $periodlist[] = $temperiod;
                                    }
                                }

                            } else {

                                foreach ($thead->children(0)->children() as $thkey => $th) {
                                    if ($thkey > 0 && $thkey <= 5) {
                                        $tempstat = new Statistic();
                                        $tempstat->label = trim($th->plaintext);
                                        $tempstat->symbol = $symbol->id;
                                        $yearflag = explode("/", $tempstat->label);
                                        $tempstat->year = (int)$yearflag[count($yearflag) - 1] - 543;
                                        $statisticlist[] = $tempstat;
                                    }
                                }
                            }
                        }

                        echo count($periodlist) . ":" . count($statisticlist) . "\n";
                        $fitcondition = true;
                        $paid = 0;
                        foreach ($table->find("tr") as $trkey => $tr) {
                            if ($trkey >= 2 && $trkey <= 12) {
                                //echo $tr->children(0)->outertext . "\n";
                                switch ($trkey) {
                                    case 2:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                //var_dump($periodlist[$tdkey - 1]);
                                                try {
                                                    $periodlist[$tdkey - 1]->asset = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                                } catch (\Exception $e) {
                                                    echo $e->getMessage();
                                                }
                                            }
                                        }
                                        break;
                                    case 3:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->debt = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 4:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->shareholder = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                                if ($tdkey == 4) {
                                                    if ($periodlist[$tdkey - 1]->debt > $periodlist[$tdkey - 1]->shareholder) {
                                                        $fitcondition = false;
                                                        echo "Debt > Shareholder\n";
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case 5:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->paid = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));

                                                if ($paid == 0) {
                                                    $paid = $periodlist[$tdkey - 1]->paid;
                                                } else {
                                                    if ($paid != $periodlist[$tdkey - 1]->paid) {
                                                        $fitcondition = false;
                                                        echo "Difference  paid\n";
                                                    }
                                                }

                                            }
                                        }
                                        break;
                                    case 6:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->income = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 7:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->profit = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 8:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->eps = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 10:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->roa = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 11:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->roe = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 12:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0 && $tdkey < 5) {
                                                $periodlist[$tdkey - 1]->margin = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }

                            }

                            if ($trkey >= 14 && $trkey <= 20) {
                                //echo $tr->children(0)->outertext . "\n";
                                switch ($trkey) {
                                    case 14:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->lastest_price = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 15:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->market_cap = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 16:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->stat_at = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 17:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->pe = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 18:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->pvb = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 19:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->vps = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    case 20:
                                        foreach ($tr->children() as $tdkey => $td) {
                                            if ($tdkey != 0) {
                                                $statisticlist[$tdkey - 1]->dividend = trim(preg_replace("/&nbsp;|,/", "", $tr->children($tdkey)->innertext));
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                        }
                        if ($fitcondition) {
                            foreach ($periodlist as $period) {
                                //var_dump($period);
                                //exit;
                                $period->save();
                            }
                            foreach ($statisticlist as $stat) {
                                $stat->save();
                            }
                            //exit;
                            echo "Condition Passed.\n";
                        } else {
                            echo "Condition Mismatch.\n";
                        }
                    }
                }
            } catch (\Exception $e) {
                echo $e->getLine() . ":" . $e->getMessage();
            }
        }

    }
}
