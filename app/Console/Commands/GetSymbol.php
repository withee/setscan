<?php

namespace App\Console\Commands;

use App\Sector;
use App\Symbol;
use Illuminate\Console\Command;

class GetSymbol extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:getsymbol';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = "http://marketdata.set.or.th/mkt/sectorquotation.do?sector=SET50&language=th&country=TH";
        $html = new \Htmldom($url);
        //echo $html->outertext;
        foreach ($html->find("select[name=sector]") as $elements) {
            $main = "none";
            foreach ($elements->children as $key => $child) {
                $sub = $child->value;
                if ($key != 0) {
                    //echo $child->innertext . "\n";
                    if (!preg_match("/--/", $child->innertext)) {
                        //echo $child->innertext . " new sub\n";
                        $main = $child->value;
                    }
                    //echo $child->value.":". $child->innertext . "\n";
                    $sector = Sector::firstOrCreate(['mainsector' => $main, 'subsector' => $sub]);
                    $sector->type = ($main == $sub) ? "main" : "sub";
                    if ($sector->save()) {
                        //echo "update " . $sub . "\n";
                    }
                }
            }
        }


        $sectors = Sector::where('type', '=', 'sub')->get();
        //echo count($sectors);
        foreach ($sectors as $sector) {
            echo $sector->subsector . "\n";
            $url = "http://marketdata.set.or.th/mkt/sectorquotation.do?sector={$sector->subsector}&language=th&country=TH";
            $html = new \Htmldom($url);
            foreach ($html->find(".table-info") as $key => $table) {
                //echo $key."\n";
                if ((int)$key > 1) {
                    //echo $table->children(1)->outertext . "\n";
                    foreach ($table->children(1)->children() as $tr) {
                        echo $tr->children(0)->children(0)->innertext . "\n";
                        $name = $tr->children(0)->children(0)->innertext;
                        $name = trim($name);
                        $symbol = Symbol::firstOrCreate(['sector' => $sector->id, 'name' => $name]);
                        $symbol->sector = $sector->id;
                        if ($symbol->save()) {

                        }
                    }
                    //exit;
                }
            }
        }

    }
}
