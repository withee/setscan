<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("symbol")->default(0);
            $table->string("year", 16)->default("0000");
            $table->string("label", 64)->default("");
            $table->double("lastest_price", 16, 2)->default(0.00);
            $table->double("market_cap", 20, 2)->default(0.00);
            $table->string("stat_at")->default("");
            $table->double("pe", 16, 2)->default(0.00);
            $table->double("pvb", 16, 2)->default(0.00);
            $table->double("vps", 16, 2)->default(0.00);
            $table->double("dividend", 16, 2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statistic');
    }
}
