<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("symbol")->default(0);
            $table->string("year",16)->default("0000");
            $table->string("label",64)->default("งบปี");
            $table->double("asset",20,2)->default(0.00);
            $table->double("debt",20,2)->default(0.00);
            $table->double("shareholder",20,2)->default(0.00);
            $table->double("paid",20,2)->default(0.00);
            $table->double("income",20,2)->default(0.00);
            $table->double("profit",20,2)->default(0.00);
            $table->double("eps",20,2)->default(0.00);
            $table->double("roa",20,0)->default(0.00);
            $table->double("roe",20,2)->default(0.00);
            $table->double("margin",20,2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('period');
    }
}
