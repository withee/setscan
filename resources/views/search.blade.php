<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        /*.container {*/
            /*text-align: center;*/
            /*display: table-cell;*/
            /*vertical-align: middle;*/
        /*}*/

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        {{--<div class="title">Laravel 5</div>--}}
        <form class="form-inline" action="/" method="get">
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail3">PE</label>
                <input type="text" name="pe"  class="form-control" id="exampleInputEmail3" placeholder="PE">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword3">P/BV</label>
                <input type="text" name="pbv" class="form-control" id="exampleInputPassword3" placeholder="P/BV">
            </div>
            <button type="submit" class="btn btn-default">find</button>
        </form>
        <div>
            ข้อมูล ณ วันที่ {{$day}}
        </div>
        <div>
            @foreach($list as $symbol)
                <p style="font-weight: bold; text-align: left;">
                    == {{$symbol->name}} : {{ $symbol->pe }} <br>
                </p>
            @endforeach
        </div>
    </div>


</div>
</body>
</html>
